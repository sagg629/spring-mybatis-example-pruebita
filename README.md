# README #

Sample application that connects to a PGSQL database and perform CRUD operations using Maven and Mybatis. Also contains ANGULARJS integration.
Components: a POJO, a mapper and a service.

Sources / tutorial: 
http://hmkcode.com/mybatis-spring-xml-annotation-mapper/
https://github.com/hmkcode/Spring-Framework/tree/master/spring-mybatis
http://postgresintl.blogspot.com/2014/04/using-postgresql-jdbc-with-spring.html
AngularJS tutorials from CodeSchools

Created 02/feb/2015
Finished 13/feb/2015
Retaken 01/jul/2015 (reason: set project in my laptop, preparative to final-degree project)

NOTE: there are some 'machetes', mostly retrieving data.
