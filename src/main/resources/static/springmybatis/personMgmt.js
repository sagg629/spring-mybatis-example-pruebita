/**
 * In this file there's an error (in other words, a "machete"),
 * the modal requires ID and name of a person when it opens.
 * In this moment those data are sent through "burnt" variables,
 * rather call a service that obtain a person data by its ID.
 * In other moment I'll change that, for now I prefer let that
 * behaviour quiet.
 *
 * Created by Sergio A Guzman <sagg629@gmail.com> on 10/02/15 02:24 PM
 */

var app = angular.module('persons', ['ui.bootstrap']);
var urlServicesPerson = "http://" + window.location.host + "/services/person";

//default/general controller
app.controller('PersonListController', function ($http, $scope, $modal, $log) {
    //default state: get only active persons (statusid = true)
    $http({url: urlServicesPerson, method: 'GET'})
        .success(function (data) {
            var activeUsers = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i].statusid == true) {
                    activeUsers.push(data[i]);
                }
            }
            $scope.persons = activeUsers;
        });

    //show active and inactive persons
    $scope.showInactivePersons = function () {
        $http({url: urlServicesPerson, method: 'GET'})
            .success(function (data) {
                var activeUsers = [];
                for (var i = 0; i < data.length; i++) {
                    if (data[i].statusid == true) {
                        activeUsers.push(data[i]);
                    }
                }
                if (showInactive.checked) {
                    $scope.persons = data; //get ALL users
                } else {
                    $scope.persons = activeUsers; //get only INACTIVE users
                }
            });
    };

    //angularJS modal
    $scope.items = [];
    $scope.open = function (pId, pNa) {
        $scope.items = [];
        $scope.items.push(pId, pNa);
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });
        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            //when window is open, update persons list
            $http({url: urlServicesPerson, method: 'GET'})
                .success(function (data) {
                    var activeUsers = [];
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].statusid == true) {
                            activeUsers.push(data[i]);
                        }
                    }
                    if (showInactive.checked) {
                        $scope.persons = data; //get ALL users
                    } else {
                        $scope.persons = activeUsers; //get only INACTIVE users
                    }
                });
        }, function () {
            $log.info("Modal closed, actual date/time: " + new Date());
        });
    };
    //angularJS modal end

    //change statusID of a person
    $scope.changeStatus = function (personId, personStatusId) {
        $http.put(urlServicesPerson + "/cst/" + personId, {})
            .success(function (response, status, headers, config) {
                console.log("Updated Person status successfully / " + status + " / " + showInactive.checked);
                $http({url: urlServicesPerson, method: 'GET'})
                    .success(function (data) {
                        var activeUsers = [];
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].statusid == true) {
                                activeUsers.push(data[i]);
                            }
                        }
                        if (showInactive.checked) {
                            $scope.persons = data; //get ALL users
                        } else {
                            $scope.persons = activeUsers; //get only INACTIVE users
                        }
                    });
            })
            .error(function (response, status, headers, config) {
                console.log("Error updating a Person / " + status);
            });
    };
});

//controller that handles an instance of the modal
app.controller("ModalInstanceCtrl", function ($scope, $modalInstance, items, $http) {
    $scope.items = items;
    $scope.selected = {
        item: $scope.items
    };
    //click on OK modal button / get written new-name and change it via Spring-service
    $scope.ok = function () {
        $scope.personId = $scope.selected.item[0];
        $scope.newPersonName = angular.element(document.querySelector("#nPersonName")).val();
        $http.put(urlServicesPerson + "/" + $scope.personId, {"name": $scope.newPersonName})
            .success(function (response, status, headers, config) {
                console.log("Updated Person successfully / " + status);
                $modalInstance.close($scope.newPersonName);
            })
            .error(function (response, status, headers, config) {
                console.log("Error updating a Person / " + status);
            });
    };
    //click on CANCEL modal button
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});