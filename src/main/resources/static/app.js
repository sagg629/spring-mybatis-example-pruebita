/**
 * Created by Sergio A Guzman <sagg629@gmail.com> on 6/02/15 11:22 AM
 */

//param1: 'store' is the application name
//param2: the array [] contains dependences/libraries
var app = angular.module('store', []);

//in a controller I define my app behaviour
//through functions and values

//controller that handles all the products
app.controller('StoreController', function () {
    this.products = gems;
});

var gems = [
    {
        name: "Monitor LG de 20 pulgadas",
        price: 135000,
        description: "Espectacular monitor FULL HD marca LG de 20 pulgadas.",
        canPurchase: false,
        soldOut: false,
        images: [{
            full: '../img/monitor_lg.jpg',
            thumb: '../img/monitor_lg.jpg'
        }],
        specifications: "Marca: LG. Pulgadas: 20. Color: negro.",
        reviews: [
            {
                stars: 5,
                body: "Muy buena nitidez",
                author: "sagg629@gmail.com"
            },
            {
                stars: 5,
                body: "Sharp and true image",
                author: "mikkel@gmail.com"
            }
        ]
    },
    {
        name: "Smartphone Samsung",
        price: 420000,
        description: "Samsung Galaxy <modelo>",
        canPurchase: true,
        soldOut: false,
        images: [{
            full: '../img/samsung_smartphone.png',
            thumb: '../img/samsung_smartphone.png'
        }],
        specifications: "CPU: Quad core Snapdragon. Memoria interna: 32GB.",
        reviews: [
            {
                stars: 5,
                body: "Excelente, muy rápido",
                author: "sagg629@gmail.com"
            }
        ]
    },
    {
        name: "Nevera Haceb",
        price: 840000,
        description: "Gran nevera no-frost con dispensador de aguita.",
        canPurchase: true,
        soldOut: true,
        images: [{
            full: '../img/nevera_haceb.jpg',
            thumb: '../img/nevera_haceb.jpg'
        }],
        specifications: "Color: plateado. Pies: muchos.",
        reviews: [
            {
                stars: 5,
                body: "Elegante, fina.",
                author: "sagg629@gmail.com"
            },
            {
                stars: 5,
                body: "Alta, finísima, además tiene ISO 9000 :D",
                author: "jaime@gmail.com"
            }
        ]
    },
    {
        name: "Horno microondas General Electric",
        price: 1400000,
        description: "Horno microondas, bueno bonito y barato",
        canPurchase: false,
        soldOut: false,
        images: [{
            full: '../img/horno_microondas.jpg',
            thumb: '../img/horno_microondas.jpg'
        }],
        specifications: "Color: plateado.",
        reviews: [
            {
                stars: 5,
                body: "Excelente, elegante, funcional... pero CAARO el desgraciado.",
                author: "otro_tipo@gmail.com"
            },
            {
                stars: 2,
                body: "AAAAAY CARAMBA!",
                author: "el_barto@gmail.com"
            },
            {
                stars: 1,
                body: "NOOOOOOOOO NI MADRES",
                author: "sagg629@gmail.com"
            }
        ]
    }
];

//controller that handles the tabs
app.controller("PanelController", function () {
    this.tab = 1;
    this.selectTab = function (setTab) {
        this.tab = setTab;
    };
    this.isSelected = function (checkTab) {
        return this.tab === checkTab;
    };
});

//controller that allow form to be submitted
app.controller("NewProducts", function () {
    this.addReview = function (product) {
        console.log(JSON.stringify(product));
        gems.push({
            name: product.prod_name,
            price: product.prod_price,
            description: product.prod_description,
            canPurchase: false,
            soldOut: false,
            images: [{
                full: '',
                thumb: ''
            }],
            specifications: product.prod_specifications,
            reviews: [
                {
                    stars: product.prod_comment_stars,
                    body: product.prod_comment_msg,
                    author: product.prod_comment_author
                }
            ]
        });
    };
});