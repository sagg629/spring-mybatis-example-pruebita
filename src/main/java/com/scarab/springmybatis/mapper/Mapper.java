package com.scarab.springmybatis.mapper;

import com.scarab.springmybatis.resources.Person;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

/**
 * Mapper is the place where we are going to link to our
 * SQL statement written within xml file or in annotations.
 * If you need to write SQL statement in XML, then you need to
 * add the xml file in the same class path as the java interface.
 * <p/>
 * DIRECT BD operations via annotations or XML.
 *
 * @author Sergio A Guzman <sagg629@gmail.com>
 * @date 2/02/15 10:07 AM
 */
public interface Mapper {
    /**
     * Retrieve list of person
     *
     * @return person list
     */
    @Select("SELECT * FROM person ORDER BY id ASC")
    public List<Map<Object, Object>> selectAllPersons();

    /**
     * Retrieve a person by its ID param
     *
     * @param id person ID
     * @return person
     */
    @Select("SELECT * FROM person WHERE id=#{id}")
    public Person getPersonById(@Param("id") int id);

    /**
     * Insert a new person
     *
     * @param person name
     * @return # of affected rows
     */
    @Insert("INSERT INTO person(name) VALUES (#{name})")
    public int insertPerson(Person person);

    /**
     * Update an existent person
     *
     * @param person object/POJO
     * @return # of affected rows
     */
    @Update("UPDATE person SET name= #{name} WHERE id= #{id}")
    public int updatePerson(Person p);

    /**
     * Soft-Delete an existent person
     * NOTE: soft-deletion
     * is given by a <statusId> param
     * false = inactive / true = active
     * This method allows toggle between active and inactive
     *
     * @param person ID
     * @return # of affected rows
     */
    @Update("UPDATE person SET statusid= " +
            "CASE WHEN statusid=true THEN false " +
            "ELSE true " +
            "END " +
            "WHERE id= #{id}")
    public int toggleStatusPerson(@Param("id") int id);
}