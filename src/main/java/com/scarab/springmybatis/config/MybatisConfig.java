package com.scarab.springmybatis.config;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

/**
 * JAVA representation of old <spring-config.xml> file
 * Configuration of: dataSource, resources/POJOs, mappers, etc.
 *
 * @author Sergio A Guzman <sagg629@gmail.com>
 * @date 4/02/15 03:41 PM
 */
@Configuration
@MapperScan("com.scarab.springmybatis.mapper")
public class MybatisConfig {
    /**
     * Database configuration
     */
    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/BDTest1");
        dataSource.setUsername("sergioguzman");
        dataSource.setPassword("Sergio629");
        return dataSource;
    }

    /**
     * Data source transaction manager
     */
    @Bean
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    /**
     * Resources / POJOs management
     */
    @Bean
    public SqlSessionFactoryBean sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setTypeAliasesPackage("com.scarab.springmybatis.resources");
        return sessionFactory;
    }

    /**
     * Mapper management
     */
    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setBasePackage("com.scarab.springmybatis.mapper");
        return mapperScannerConfigurer;
    }
}
