package com.scarab.springmybatis.config;

import com.scarab.springmybatis.Application;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Class that RUNS the project.
 *
 * @author Sergio A Guzman <sagg629@gmail.com>
 * @date 4/02/15 03:41 PM
 */
@Configuration
@EnableAutoConfiguration
@EnableTransactionManagement
@ComponentScan(basePackageClasses = Application.class)
@Import({MybatisConfig.class})
public class ApplicationConfig {
    private static Log log = LogFactory.getLog(ApplicationConfig.class);

    public static void main(String[] args) {
        //ApplicationContext cxt = new ClassPathXmlApplicationContext("spring-config.xml");
        //Service service = (Service) cxt.getBean("service");
        log.info("////// INICIO DE LA APLICACIÓN //////");

        /**
         * When I perform BD operations I may to call a method stored at Service file.
         * Communication at this stage is: APP -> SERVICE -> MAPPER -> POJO
         * and in the same mode, in reverse way
         * POJO -> MAPPER -> SERVICE -> APP
         */

        SpringApplication.run(ApplicationConfig.class, args);
    }
}
