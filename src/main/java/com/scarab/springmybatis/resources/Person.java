package com.scarab.springmybatis.resources;

/**
 * is a POJO (plain java object) to map data from/to it
 *
 * @author Sergio A Guzman <sagg629@gmail.com>
 * @date 2/02/15 10:07 AM
 */
public class Person {
    private int id;
    private String name;
    private boolean statusid;

    public boolean isStatusId() {
        return statusid;
    }

    public void setStatusId(boolean statusid) {
        this.statusid = statusid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "\n Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", statusid=" + statusid +
                '}';
    }
}
