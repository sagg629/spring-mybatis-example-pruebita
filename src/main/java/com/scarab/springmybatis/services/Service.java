package com.scarab.springmybatis.services;

import com.scarab.springmybatis.mapper.Mapper;
import com.scarab.springmybatis.resources.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * is just a helper java class wrapping Mapper.java methods.
 * Each method calls to its matching method at <Mapper> file.
 * Each method should return a ResponseEntity object that contains:
 * param1: an String, object or other, param2: a HTTP status (500, 200, 404, etc.)
 *
 * @author Sergio A Guzman <sagg629@gmail.com>
 * @date 2/02/15 10:07 AM
 */

@RestController
//this is the URL to enter to this service. append this to general URL
@RequestMapping("services/person")
public class Service {

    //define required mappers
    @Autowired
    Mapper mapper;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity selectAllPerson() {
        return new ResponseEntity<>(mapper.selectAllPersons(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "{id}")
    @ResponseBody
    public ResponseEntity selectPerson(@PathVariable(value = "id") int id) {
        return new ResponseEntity<>(mapper.getPersonById(id), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity insertPerson(@RequestBody Person person) {
        mapper.insertPerson(person);
        return new ResponseEntity<>("Added successfully", HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "{id}")
    @ResponseBody
    public ResponseEntity updatePerson(@PathVariable(value = "id") int id, @RequestBody Person person) {
        person.setId(id);
        mapper.updatePerson(person);
        return new ResponseEntity<>("Updated successfully", HttpStatus.NO_CONTENT);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/cst/{id}")
    @ResponseBody
    public ResponseEntity modifyStatusPerson(@PathVariable(value = "id") int id) {
        mapper.toggleStatusPerson(id);
        return new ResponseEntity<>("Person status updated successfully", HttpStatus.NO_CONTENT);
    }
}
